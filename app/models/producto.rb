# == Schema Information
#
# Table name: productos
#
#  id            :integer          not null, primary key
#  nombre        :string(255)      not null
#  codigo        :string(255)
#  categoria     :string(255)
#  precio_compra :float
#  precio_venta  :float
#  hasiva        :boolean
#  stock         :integer
#  tipo          :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#

class Producto < ActiveRecord::Base
	#relations
  has_many :ingreso_productos
  has_many :item_facturas
  has_many :item_pedidos
  # belongs_to :item_factura
  # has_one :kardex
  # has_many :factura_compras_productos
  # has_many :factura_compras, :through => :factura_compras_productos
  # has_many :canjes
  accepts_nested_attributes_for :ingreso_productos, :allow_destroy => true, reject_if: :all_blank
	validates :nombre, :precio_compra, :precio_venta, :presence => true
  
  def self.autocomplete(params)
    productos = Producto.where("nombre like ?", "%#{params}%")
    productos = productos.map do |producto|
    {
      :id => producto.id,
      # :label => ingreso.producto.nombre + "/" + "Lote:" + ingreso.lote ,
      :label => producto.nombre,
      :value => producto.nombre,
      :precio_venta => producto.precio_venta,
      :id_ingreso => producto.id,
      :iva => producto.hasiva
    }
    end
    productos
  end

end
