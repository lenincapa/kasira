# == Schema Information
#
# Table name: clientes
#
#  id                       :integer          not null, primary key
#  nombre                   :string(255)
#  direccion                :string(255)
#  numero_de_identificacion :string(255)
#  telefono                 :string(255)
#  email                    :string(255)
#  created_at               :datetime
#  updated_at               :datetime
#

class Cliente < ActiveRecord::Base
  has_many :facturas
	validates :nombre, :numero_de_identificacion, :presence => true
  validates_id :numero_de_identificacion, :message => "Cédula incorrecta"
  
  sync :all
	def self.autocomplete(params)
		clientes = Cliente.where("numero_de_identificacion like ?", "%#{params}%")
    clientes = clientes.map do |cliente|
      {
        :id => cliente.id,
        :label => cliente.numero_de_identificacion + " / " + cliente.nombre,
        :value => cliente.numero_de_identificacion,
        :cedula => cliente.numero_de_identificacion,
        :nombre => cliente.nombre,
        :direccion => cliente.direccion,
        :telefono => cliente.telefono
      }
    end
    clientes 
	end
end
