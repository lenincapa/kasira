# == Schema Information
#
# Table name: facturas
#
#  id                 :integer          not null, primary key
#  numero             :integer
#  fecha_de_emision   :datetime
#  fecha_de_caducidad :datetime
#  subtotal_0         :float
#  subtotal_12        :float
#  iva                :float
#  total              :float
#  descuento          :float
#  razon_anulada      :string(255)
#  tipo               :string(255)
#  anulada            :boolean          default(FALSE)
#  cliente_id         :integer
#  user_id            :integer
#  created_at         :datetime
#  updated_at         :datetime
#

class Factura < ActiveRecord::Base
	# has_many :cierre_caja_items
	belongs_to :cliente
	belongs_to :user
	has_many :item_facturas
	has_many :productos, :through => :item_facturas

	#real-time
	sync :all

	#nested
	accepts_nested_attributes_for :item_facturas, :cliente

	#valitations
	validates :cliente_id, :user_id, :numero, :subtotal_0, :subtotal_12, :descuento, :iva, :total, :presence =>true
	validates :numero, :subtotal_0, :subtotal_12, :descuento, :iva, :numericality => true, :numericality => { :greater_than_or_equal_to => 0 }
	validates :total, :numericality => { :greater_than => 0 }

	#callbacks
	before_validation :set_factura_normal, :if => :factura_normal

	#methods
	def self.money_today
		self.where(:created_at => Time.now.beginning_of_day..Time.now.end_of_day).sum(:total)
	end

	def cliente_attributes=(attributes)
		if attributes['id'].present?
			self.cliente = Cliente.find(attributes['id'])
		end
		super
	end

	def factura_normal
		self.tipo != "normal"
	end

	def set_factura_normal
		self.fecha_de_emision = Time.now
		self.fecha_de_caducidad = self.fecha_de_emision + 30.days
		self.numero = Factura.last ? Factura.last.numero + 1 : 1
		subtotal_0 = 0
		subtotal_12 = 0
		iva = 0
		self.item_facturas.each do |item|
			unless item.producto_id.nil?
				producto = Producto.find item.producto_id
				cantidad = item.cantidad
				iva_producto = (producto.precio_venta * 0.12).round(2)
				item.total = (producto.precio_venta * cantidad) #asigna valor total del item
					if producto.hasiva == true
						item.valor_unitario = (producto.precio_venta - iva_producto) #valor venta menos el iva 12
						item.iva = (item.total * 0.12).round(2)
						subtotal_12 = subtotal_12 + (item.valor_unitario * cantidad)
					else
						item.valor_unitario = producto.precio_venta
						subtotal_0 = subtotal_0 + (item.valor_unitario * cantidad)
						item.iva = 0
					end
				iva = iva + item.iva #suma iva de los productos
			end
		end
		self.iva = iva.round(2)
		self.subtotal_0 = subtotal_0
		self.subtotal_12 = subtotal_12
		self.total = subtotal_0 + subtotal_12 + self.iva
		self.total = self.total.round(2)
		self.descuento = 0
	end
	
	def self.create_items_facturas (item_pedidos)
		itemfacturas = []
		item_pedidos.each do |item|
			itemfactura = ItemFactura.new(:cantidad => item.cantidad, :producto => item.producto, :valor_unitario => item.valor_unitario, :descuento => item.descuento, :total => item.total, :iva => item.iva, :tipo => "mesa")
			itemfacturas << itemfactura
		end
		itemfacturas
	end

end
