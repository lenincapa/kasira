# == Schema Information
#
# Table name: item_pedidos
#
#  id             :integer          not null, primary key
#  cantidad       :integer
#  descuento      :float            default(0.0)
#  total          :float
#  valor_unitario :float
#  iva            :float
#  producto_id    :integer
#  pedido_id      :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class ItemPedido < ActiveRecord::Base

  attr_accessor :descripcion
  attr_accessor :precio_venta
  
  #real-time
  sync :all

	#relationships
  belongs_to :pedido
  belongs_to :producto

	# validations:
	validates :cantidad, :valor_unitario, :total, :numericality => { :greater_than_or_equal_to => 0 }
  # validate :stock

  # methods
  def descripcion
  	if self.producto
  		self.producto.nombre
  	end
  end

  def precio_venta
    if self.producto
      self.producto.precio_venta
    end
  end
  # methos class
  def self.platos_vendidos_hoy
    production = self.where(:created_at => Time.now.beginning_of_day..Time.now.end_of_day)
    cantidad_platos = production.includes(:producto).group("nombre").sum(:cantidad)    
  end
end
