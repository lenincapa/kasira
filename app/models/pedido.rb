# == Schema Information
#
# Table name: pedidos
#
#  id         :integer          not null, primary key
#  mesa_id    :integer
#  total      :float
#  facturado  :boolean          default(FALSE)
#  created_at :datetime
#  updated_at :datetime
#

class Pedido < ActiveRecord::Base
	belongs_to :mesa
	belongs_to :user
	has_many :item_pedidos, dependent: :destroy
	has_many :productos, :through => :item_pedidos
	
	#nested
	accepts_nested_attributes_for :item_pedidos
	accepts_nested_attributes_for :item_pedidos, :allow_destroy => true

	before_validation :set_values;
	# after_update :set_items

	#real-time
  sync :all
	sync_touch :mesa
	
	def facturar(cliente)
		factura  = Factura.new(:tipo => "mesa", :subtotal_0 => self.subtotal_0, :subtotal_12 => self.subtotal_12, :descuento => self.descuento, :iva => self.iva, :total => self.total)
		if cliente[:cliente_id].empty?
			cliente = Cliente.new(:nombre => cliente[:nombre], :numero_de_identificacion => cliente[:numero_de_identificacion], :direccion => cliente[:direccion], :telefono => cliente[:telefono])
			cliente.save
			factura.cliente_id = cliente.id
		else
			factura.cliente_id = cliente[:cliente_id]
		end
		factura.numero = Factura.last ? Factura.last.numero + 1 : 1
		factura.fecha_de_emision = Time.now
		factura.fecha_de_caducidad = Time.now + 30.days
		factura.item_facturas = Factura.create_items_facturas(self.item_pedidos)
		factura
	end

	def update_mesa_from_factura
		self.update(:facturado =>  true)
		self.mesa.update(:ocupada => false)
	end

	def set_values
		self.numero = Pedido.last ? Pedido.last.numero + 1 : 1
		subtotal_0 = 0
		subtotal_12 = 0
		iva = 0
		self.item_pedidos.each do |item|
			unless item.producto_id.nil?
				producto = Producto.find item.producto_id
				cantidad = item.cantidad
				# item.tipo = "menu"
				iva_producto = (producto.precio_venta * 0.12).round(2)
				item.total = (producto.precio_venta * cantidad) #asigna valor total del item
				if producto.hasiva == true
					item.valor_unitario = (producto.precio_venta - iva_producto).round(2) #valor venta menos el iva 12
					item.iva = (iva_producto * cantidad)
					subtotal_12 = subtotal_12 + (item.valor_unitario * cantidad)
				else
					item.valor_unitario = producto.precio_venta
					subtotal_0 = subtotal_0 + (item.valor_unitario * cantidad)
					item.iva = 0
				end
				iva = iva + item.iva #suma iva de los productos
			end
		end
		self.iva = iva.round(2)
		self.subtotal_0 = subtotal_0
		self.subtotal_12 = subtotal_12
		self.total = subtotal_0 + subtotal_12 + self.iva
		self.total = self.total.round(2)
		self.descuento = 0
		self.mesa.update(:ocupada => true)
	end
end
