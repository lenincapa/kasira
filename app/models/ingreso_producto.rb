# == Schema Information
#
# Table name: ingreso_productos
#
#  id              :integer          not null, primary key
#  producto_id     :integer
#  fecha_caducidad :date
#  cantidad        :integer
#  lote            :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class IngresoProducto < ActiveRecord::Base
	# has_many :item_facturas
  belongs_to :producto
  # has_many :antiguos, :foreign_key => 'antiguo_id', :class_name => "Canje"
  # has_many :nuevos, :foreign_key => 'nuevo_id', :class_name => "Canje"
end
