# == Schema Information
#
# Table name: item_facturas
#
#  id             :integer          not null, primary key
#  cantidad       :integer
#  descuento      :float            default(0.0)
#  total          :float
#  valor_unitario :float
#  iva            :float
#  tipo           :string(255)
#  factura_id     :integer
#  producto_id    :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class ItemFactura < ActiveRecord::Base
	attr_accessor :descripcion

  #callbacks
  # after_create :add_kardex_line, :disminuir_stock

  #relationships
  belongs_to :factura
  belongs_to :producto

	# validations:
	validates :cantidad, :valor_unitario, :total, :presence => true,:numericality => { :greater_than_or_equal_to => 0 }
  # validate :stock

  # methods

  def descripcion
  	if self.producto
  		self.producto.nombre
  	end
  end
end
