# == Schema Information
#
# Table name: mesas
#
#  id         :integer          not null, primary key
#  numero     :integer          not null
#  ocupada    :boolean          default(FALSE)
#  created_at :datetime
#  updated_at :datetime
#

class Mesa < ActiveRecord::Base
	has_many :pedidos
	validates :numero, :numericality => { :greater_than_or_equal_to => 0, :less_than_or_equal_to => 99, :message => "Rango maximo de 0-99" }
	validates :numero, :uniqueness => :true
	sync :all
end
