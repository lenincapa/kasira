window.Helpers ||= {}

window.Helpers.ResetFactura = {
  reseted: ->
    $(".nueva_factura").click ->
      $("table#factura > tbody > tr").not(':first').not(':last').remove()
      $(":input", "#form-factura").removeAttr("checked").removeAttr("selected").not(":button, :submit, :reset, :radio, :checkbox").val ""
      $(".total_factura").text("0.00")
      $(".factura-content").toggleClass("hovered")
      $(".animation-save").remove()
      $(".comprobante").fadeIn(1000)
}

jQuery ->
$(document).on "page:load", ->
  window.Helpers.ResetFactura.reseted()
