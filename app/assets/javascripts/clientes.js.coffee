window.Helpers ||= {}

window.Helpers.GetIdenticon = {
 	generate: ->
 		$("#identificacion").on "input", ->
 			texto = $(this).val()
 			window.Helpers.GetIdenticon.settings(texto)

	show: ->
		texto = $(".identificacion").text()
		window.Helpers.GetIdenticon.settings(texto)
	
	settings: (texto) ->
		salt = 0
		rounds = 1
		size = 110
		outputType = "HEX"
		hashtype = "SHA-512"
		shaObj = new jsSHA(texto + salt, "TEXT")
		hash = shaObj.getHash(hashtype, outputType, rounds)
		data = new Identicon(hash, size).toString()
		$("#show_identicon")[0].src = "data:image/png;base64," + data

	list_clients: ->
		$(".identificacion").each ->
			texto = $(this).text()
			image = $(this).closest(".clients").find(".img-thumbnail")
			salt = 0
			rounds = 1
			size = 110
			outputType = "HEX"
			hashtype = "SHA-512"
			shaObj = new jsSHA(texto + salt, "TEXT")
			hash = shaObj.getHash(hashtype, outputType, rounds)
			data = new Identicon(hash, size).toString()
			image[0].src = "data:image/png;base64," + data
}

jQuery window.Helpers.GetIdenticon.init
$(document).on "ready page:load", window.Helpers.GetIdenticon.generate
$(document).on "ready page:load", window.Helpers.GetIdenticon.list_clients
