window.Helpers ||= {}

window.Helpers.AutocompleteHelper = {
  init_autocomplete: ->
    $(".numero_de_identificacion").autocomplete
      minLength: 3
      source: "/clientes/autocomplete.json"
      select: (event, ui) ->
          $(".cliente_id").val ui.item.id
          $(".nombre").val ui.item.nombre
          $(".direccion").val ui.item.direccion
          $(".telefono").val ui.item.telefono
          texto = ui.item.cedula
          window.Helpers.GetIdenticon.settings(texto)

    $(".autocomplete_producto").autocomplete
      minLength: 3
      source: "/productos/autocomplete.json"
      response: (event, ui) ->
        unless ui.content.length
          NoExiste =
            id: "vacio"
            label: "No existe: #{event.target.value}"
          ui.content.push NoExiste
        else
          $("#message").empty()
      select: (event, ui) ->
        $this = $(this)
        iva_producto = (ui.item.precio_venta * 0.12).toFixed(2)
        precio_unt = (ui.item.precio_venta - iva_producto).toFixed(2)
        $this.closest(".fields").find("td:nth-child(2)").find(".producto_id").val ui.item.id_ingreso
        $this.closest(".fields").find("td:nth-child(2)").find(".hasiva").val ui.item.iva
        $this.closest(".fields").find("td:nth-child(4)").find(".iva").val(iva_producto)
        $this.closest(".fields").find("td:nth-child(5)").find(".precio_venta").val(ui.item.precio_venta)
        if ui.item.iva == false
          $this.closest(".fields").find("td:nth-child(4)").find(".iva").val("0.0")
          $this.closest(".fields").find("td:nth-child(3)").find(".valor_unitario").val ui.item.precio_venta
        else
          $this.closest(".fields").find("td:nth-child(3)").find(".valor_unitario").val precio_unt

    $(".cantidad").on "input", ->
      $this = $(this)
      window.Helpers.AutocompleteHelper.calcular_total_producto($this)
      window.Helpers.AutocompleteHelper.calcular_valores_factura()

  calcular_total_producto: (componente) ->
    cantidad = componente.closest(".fields").find("td:nth-child(2)").find(".cantidad").val()
    cantidad = parseInt cantidad
    valor_unitario = componente.closest(".fields").find("td:nth-child(3)").find(".valor_unitario").val()
    valor_unitario = parseFloat(valor_unitario)
    value_iva = componente.closest(".fields").find("td:nth-child(4)").find(".iva").val()
    hasiva = componente.closest(".fields").find("td:nth-child(2)").find(".hasiva").val()
    if hasiva == "true" || (hasiva == "" && value_iva != "0.0")
      precio_venta  = componente.closest(".fields").find("td:nth-child(5)").find(".precio_venta").val()
      iva_producto = (precio_venta * 0.12).toFixed(2)
      iva_suma = (cantidad * iva_producto).toFixed(2)
      componente.closest(".fields").find("td:nth-child(4)").find(".iva").val(iva_suma)
      total_produto = ((cantidad * valor_unitario) + parseFloat(iva_suma)).toFixed(2)
      componente.closest(".fields").find("td:nth-child(5)").find(".total_item").val(total_produto)
    else
      total_produto = (cantidad * valor_unitario).toFixed(2)
      componente.closest(".fields").find("td:nth-child(5)").find(".total_item").val(total_produto)

  calcular_valores_factura: ->
    sum = 0
    sum_iva = 0
    sum_12 = 0
    sum_0 = 0
    $(".total_item").each ->
      if $(this).val() and $(this).is(":visible")
        sum += parseFloat($(this).val())
    $(".iva").each ->
      if $(this).val() and $(this).is(":visible")
        iva = parseFloat($(this).val())
        sum_iva += iva
        valor_a_sumar = parseFloat($(this).closest(".fields").find("td:nth-child(5)").find(".total_item").val())
        if iva != 0
          sum_12 += valor_a_sumar
        else
          sum_0 += valor_a_sumar
    $(".subtotal_0").text sum_0.toFixed(2)
    $(".subtotal_12").text sum_12.toFixed(2)
    $(".descuento_factura").text 0
    $(".iva_factura").text(sum_iva.toFixed(2));
    $(".total_factura").text("$" + (sum).toFixed(2));
}

jQuery window.Helpers.AutocompleteHelper.init
$(document).on "ready page:load", window.Helpers.AutocompleteHelper.init_autocomplete

$(document).on "nested:fieldAdded", window.Helpers.AutocompleteHelper.init_autocomplete
$(document).on "nested:fieldRemoved", window.Helpers.AutocompleteHelper.calcular_valores_factura

