$ ->
  flashCallback = ->
    $(".alert").slideUp ->
    	$(this).remove()
  setTimeout flashCallback, 3000

$(document).on "ready page:load", ->
  $(".card").click ->
    $(this).toggleClass "flipped"

$ ->
  $.setAjaxPagination = ->
  	$(".pagination a").on "click", ->
  		$.get @href, null, null, "script"
  		false

  $.setAjaxPagination()