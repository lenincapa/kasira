class MesasController < ApplicationController
  before_filter :require_login
  enable_sync

	def index
		@mesa= Mesa.new
		@mesas = Mesa.all
		respond_to do |format|
      format.html
      format.js { render json: @mesas }
    end
	end

	def new
		@mesa= Mesa.new
		respond_to do |format|
      format.html
      format.js
    end
	end

	def create
		@mesa = Mesa.new(mesa_params)
		@mesa.save
		respond_to do |format|
			format.html
			format.js
		end
	end
	
  def edit
    @mesa = Mesa.find(params[:id])
    fresh_when @mesa
  end

  def update
    @mesa = Mesa.find(params[:id])
    respond_to do |format|
      if @mesa.update_attributes(mesa_params)
        format.html { redirect_to @mesa, notice: 'mesa was successfully updated.' }
        format.js { redirect_via_turbolinks_to @mesa, notice: 'Mesa was successfully updated.' }
      else
        format.html { render action: "edit" }
        format.js { render action: "edit" }
      end
    end
	end

	private
	def set_mesa
		@mesa = Mesa.find(params[:id])
	end

	def mesa_params
		params.require(:mesa).permit(:numero)
	end
end
