class ProductosController < ApplicationController
  before_filter :require_login
  before_action :set_producto, only: [:show, :edit, :update]
  before_action :find_caducados, only: [:caducado, :alerta]
  
  def index
    # ['Plato Fuerte','Ceviche','Sopa','Bebida','Postre','De la casa', 'Desayuno','Almuerzo', 'Otros']
    @menu = Producto.where(:tipo => "menu")
    @plato_fuertes , @ceviches, @sopas, @bebidas, @postres, @casa = [], [], [], [], [], []
    @menu.each do |item|
      @plato_fuertes << item if item.categoria == 'Plato Fuerte'
      @ceviches << item if item.categoria == 'Ceviche'
      @sopas << item if item.categoria == 'Sopa'
      @bebidas << item if item.categoria == 'Bebida'
      @postres << item if item.categoria == 'Postre'
      @casa << item if item.categoria == 'De la casa'
    end
    respond_to do |format|
      format.html
      # format.json { render json: ProductosDatatable.new(view_context) }
    end
  end

  def menu
    @menu = Producto.where(:tipo => "menu")
  end

  def other
    @producto = Producto.where(:tipo => "producto")
  end

  def autocomplete
    respond_to do |format|
      format.json { render :json => Producto.autocomplete(params[:term]) }
    end    
  end

  def new
    @producto=  Producto.new
    @producto.tipo = "producto"
    @producto.ingreso_productos.build
    respond_to do |format|
      format.js{ render "new_or_edit" }
    end
  end

  def new_menu
    @producto=  Producto.new
    @producto.tipo = "menu"
    @producto.ingreso_productos.build
    respond_to do |format|
      format.js{ render "new_or_edit" }
    end
  end

  def show
    respond_to do |format|
      format.js
    end
  end
  
  def edit
    respond_to do |format|
      format.js{ render "new_or_edit" }
    end
  end

  def create
    @producto = Producto.new(producto_params)
    respond_to do |format|
      @producto.save
      format.js{ render "success" }
    end
  end

  def update
    respond_to do |format|
      @producto.update(producto_params)
        format.js{ render "success" }
    end
  end

  # def caducado
  # end

  # def alerta
  #   if @caducados.empty? == true
  #     redirect_to root_path
  #   end    
  # end

  private 
  
  def producto_params
    params.require(:producto).permit :nombre,
    :codigo,
    :categoria,
    :precio_venta,
    :precio_compra,
    :hasiva,
    :tipo,
    :ingreso_productos_attributes => [
      :lote,
      :cantidad,
      :fecha_caducidad,
      :producto_id,
      :_destroy,
      :id,
    ]                                    
  end  
  
  def set_producto
      @producto = Producto.find(params[:id])
  end

  def find_caducados
    @caducados = IngresoProducto.where(:fecha_caducidad =>Time.now.end_of_day..Time.now.months_since(4)).where("cantidad != '0'")
    @hoy = Date.today    
  end
end
