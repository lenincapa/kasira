class ClientesController < ApplicationController
  before_filter :require_login
  before_action :find_cliente, only: [:show, :edit, :update, :destroy]
	enable_sync
	def index
    # @clientes = Cliente.order("created_at").page(params[:page]).per_page(6)
    @clientes = Cliente.all
    respond_to do |format|
      format.html
      format.js { render json: @clientes }
    end
  end

  def autocomplete
    respond_to do |format|
      format.json { render :json => Cliente.autocomplete(params[:term]) }
    end
  end

	def new
		@cliente= Cliente.new
    respond_to do |format|
      format.html
      format.js { render json: @cliente }
    end
	end

	def create
    @cliente = Cliente.new(cliente_params)
    respond_to do |format|
      @cliente.save
      format.html { redirect_to clientes_path, :notice => "Almacendao" }
      format.js { render "success" }
    end
	end
  
  def show
    respond_to do |format|
      format.js{ render "show" }
    end
  end


  def edit
    respond_to do |format|
      format.js{ render "new_or_edit" }
    end
  end

  def update
    respond_to do |format|
      @cliente.update(cliente_params)
      format.js { render "success"}
      format.json { respond_with_bip(@cliente) }
    end
  end

  def best_clients
    @clientes = Cliente.all
    respond_to do |format|
      format.js
    end
  end

	private
	def find_cliente
		@cliente = Cliente.find(params[:id])
	end

	def cliente_params
		params.require(:cliente).permit(:nombre, :direccion, :numero_de_identificacion, :telefono, :email)
	end
end
