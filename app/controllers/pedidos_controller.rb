class PedidosController < ApplicationController
	before_filter :require_login
	before_action :find_mesa, only: [:new, :create, :edit, :update]
	before_action :set_pedido, only: [:edit, :update, :facturar]
  
  enable_sync

	def new
		@pedido = Pedido.new
		@pedido.item_pedidos.build
		respond_to do |format|
			format.html
			format.js{ render json: @pedido }
		end	
	end

	def show
		respond_to do |format|
			format.html
			format.js
		end
	end

	def create
		@pedido = Pedido.new(pedido_params.merge(user_id: current_user.id))
		@pedido.mesa = @mesa
		@pedido.save
		respond_to do |format|
			format.html {redirect_to root_path, :notice => "almacenado"}
			format.js{ render json: @pedido }
		end
	end

	def edit
    respond_to do |format|
			format.html
			format.js{ render json: @pedido }
    end
  end
  
  def update
    respond_to do |format|
      @pedido.update(pedido_params)
			format.html {redirect_to root_path, :notice => "Almacenado"}
			format.js{ render json: @pedido }
    end
  end
	
	def facturar
	  respond_to do |format|
			format.html
      format.js
    end
	end

	def facturado
		@pedido = Pedido.find(params[:id])
		@factura = @pedido.facturar(@_params)
		@factura.user = current_user
		if @factura.save
			@pedido.update(:facturado =>  true)
			@pedido.mesa.update(:ocupada => false)
			redirect_to facturas_path, :notice => "Factura Creada"
		else
			redirect_to facturas_path
			flash[:error] = 'Error al crear la factura'
		end
	end

	private

	def pedido_params
		params.require(:pedido).permit :mesa_id,
		:item_pedidos_attributes => [
			:descripcion,
			:precio_venta,
			:cantidad,
			:valor_unitario,
			:iva,
			:total,
			:producto_id,
			:id,
			:pedido_id,
			:_destroy
		]
	end
	
	def find_mesa
		# @mesa = Mesa.find_by_numero(params[:numero])
		@mesa = Mesa.find(params[:mesa_id])
	end

	def set_pedido
		# @pedido = Pedido.find_by_numero(params[:id])
		@pedido = Pedido.find(params[:id])
	end
end
