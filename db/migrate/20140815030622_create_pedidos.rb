class CreatePedidos < ActiveRecord::Migration
  def change
    create_table :pedidos do |t|
    	t.integer :numero
    	t.float :subtotal_0
    	t.float :subtotal_12
    	t.float :iva
    	t.float :total
    	t.float :descuento
    	t.boolean :facturado, default: false
    	t.references :mesa, index: true
    	t.references :user, index: true
      t.timestamps
    end
  end
end
