class CreateProductos < ActiveRecord::Migration
  def change
    create_table :productos do |t|
      t.string :nombre, :null => false
      t.string :codigo
      t.string :categoria
      t.float :precio_compra
      t.float :precio_venta
      t.boolean :hasiva
      t.integer :stock
      t.string :tipo
      t.timestamps
    end
  end
end
