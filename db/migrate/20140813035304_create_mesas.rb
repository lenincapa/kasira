class CreateMesas < ActiveRecord::Migration
  def change
    create_table :mesas do |t|
    	t.integer :numero, null: false
    	t.boolean :ocupada, default: false
      t.timestamps
    end
  end
end
