class CreateItemPedidos < ActiveRecord::Migration
  def change
    create_table :item_pedidos do |t|
			t.integer :cantidad
			t.float :descuento, :default => 0.00
			t.float :total
			t.float :valor_unitario
			t.float :iva
			t.references :producto, index: true    	
    	t.references :pedido, index: true
      t.timestamps
    end
  end
end
