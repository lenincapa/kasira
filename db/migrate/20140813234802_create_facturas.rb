class CreateFacturas < ActiveRecord::Migration
  def change
    create_table :facturas do |t|
    	t.integer :numero
    	t.datetime :fecha_de_emision
    	t.datetime :fecha_de_caducidad
    	t.float :subtotal_0
    	t.float :subtotal_12
    	t.float :iva
    	t.float :total
    	t.float :descuento
      t.string :razon_anulada
      t.string :tipo
      t.boolean :anulada, default: false
      t.references :cliente, index: true
      t.references :user, index: true
      t.timestamps
    end
  end
end
