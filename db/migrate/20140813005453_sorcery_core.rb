class SorceryCore < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username, :null => false
      t.string :crypted_password, :null => false
      t.string :salt, :null => false
      t.boolean :suspendido, :default => false
      t.references :cliente, index: true
      t.timestamps
    end
    # add_index :users, :email, unique: true
  end
end