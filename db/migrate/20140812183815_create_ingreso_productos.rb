class CreateIngresoProductos < ActiveRecord::Migration
  def change
    create_table :ingreso_productos do |t|
    	t.references :producto, index: true
    	t.date :fecha_caducidad
    	t.integer :cantidad
    	t.string :lote
      t.timestamps
    end
  end
end
