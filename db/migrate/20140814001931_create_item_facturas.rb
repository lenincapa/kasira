class CreateItemFacturas < ActiveRecord::Migration
	def change
		create_table :item_facturas do |t|
			t.integer :cantidad
			t.float :descuento, :default => 0.00
			t.float :total
			t.float :valor_unitario
			t.float :iva
			t.string :tipo
			t.references :factura, index: true
			t.references :producto, index: true
			t.timestamps
		end
	end
end
